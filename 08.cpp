#include <cstdio>
#include <cmath>
#include <typeinfo>

#define HMNOZNIK 0.9
#define HPOCZATEK 0.1
#define MAX_ITERACJI 400
#define TYP double

template <class T>
T log10(T x) {
    return log(x) / log((T) 10.0);
}


template <class T>
class Pochodna {
	public:
		void policz();
		Pochodna() {
			h = (T) 0.1;
			poczatek = (T) 0.0;
			srodek = (T) pi() / (T)4.0;
			koniec = (T) pi() / (T) 2.0;
		}
	private:
		T h;

		T poczatek;
		T srodek;
		T koniec;

		T f(T x);
		T forward(T x, T h);
		T central(T x, T h);
		T backward(T x, T h);
		T forward_3p(T x, T h);
		T backward_3p(T x, T h);

		inline T pi() { 
			return std::atan((T)1.0) * (T)4.0;
		}
};


template <class T>
T Pochodna<T>::f(T x) {
    return cos(x);
}


template <class T>
T Pochodna<T>::forward(T x, T h) {
    return (f(x + h) - f(x)) / h;
}


template <class T>
T Pochodna<T>::central(T x, T h) {
    return (f(x + h) - f(x - h)) / ((T) 2.0 * h);
}


template <class T>
T Pochodna<T>::backward(T x, T h) {
    return (f(x) - f(x - h)) / h;
}


template <class T>
T Pochodna<T>::forward_3p(T x, T h) {
    return (- (T)0.5 * f(x + (T)2.0 * h) + (T)-1.5 * f(x) + (T)2.0 * f(x + h) )
        / h;
}


template <class T>
T Pochodna<T>::backward_3p(T x, T h) {
    return ((T)0.5 * f(x - (T)2.0 * h) - (T)2.0 * f(x - h) + (T)1.5 * f(x))
        / h;
}


template <class T>
void Pochodna<T>::policz() {
    printf("Przybliżone wartości pochodnych funkcji dla h = 1e-2\n");
    h = 1e-2;
    printf( "%10s %10s %10s %10s %10s %10s %10s %10s %10s \n", "x=0","x=0", "x=pi/4", "x=pi/4", "x=pi/4", "x=pi/4", "x=pi/4", "x=pi/2", "x=pi/2");
    printf( "%10s %10s %10s %10s %10s %10s %10s %10s %10s \n", "forward", "forward3", "backward", "backward3", "central", "forward", "forward3", "backward", "backward3");
    printf( "%10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf\n",
            forward(poczatek, h),
            forward_3p(poczatek, h),
            backward(srodek, h),
            backward_3p(srodek, h),
            central(srodek, h),
            forward(srodek, h),
            forward_3p(srodek, h),
            backward(koniec, h),
            backward_3p(koniec, h));

    printf( "%10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf %10.6lf\n",
            (log10(fabs(forward(poczatek, 2.0 * h))) - log10(fabs(forward(poczatek, h)))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(forward_3p(poczatek, 2.0 * h))) - log10(fabs(forward_3p(poczatek, h)))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(backward(srodek, 2.0 * h) + (T) sqrt((T)2.0) / (T) 2.0)) - log10(fabs(backward(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(backward(srodek, 2.0 * h) )) - log10(fabs(backward(srodek, h)))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(backward_3p(srodek, 2.0 * h) + (T) sqrt((T)2.0) / (T) 2.0)) - log10(fabs(backward_3p(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(central(srodek, 2.0 * h) + (T) sqrt((T)2.0) / (T) 2.0)) - log10(fabs(central(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(forward(srodek, 2.0 * h) + (T) sqrt((T)2.0) / (T) 2.0)) - log10(fabs(forward(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(forward_3p(srodek, 2.0 * h) + (T) sqrt((T)2.0) / (T) 2.0)) - log10(fabs(forward_3p(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(backward(koniec, 2.0 * h) + (T) 1.0)) - log10(fabs(backward(koniec, h) + (T) 1.0))) / (log10(2.0 * h) - log10(h)),
            (log10(fabs(backward_3p(koniec, 2.0 * h) +(T) 1.0)) - log10(fabs(backward_3p(koniec, h) +(T) 1.0))) / (log10(2.0 * h) - log10(h))
            );

    // Wygenerowanie danych przedstawiających błędy bezwzględne
    // różnic skończonych w zależności od kroku h do pliku "plot.dat"
    FILE *file = fopen("plot.dat", "w");

    int i;
    h = HPOCZATEK;
    for(i = 0; i < MAX_ITERACJI; i++, h *= HMNOZNIK) {
        fprintf(file,
                "%.20lf %.20lf %.20lf %.20lf %.20lf %.20lf %.20lf %.20lf %.20lf %.20lf\n",
                h,
                fabs(forward(poczatek, h)),
                fabs(forward_3p(poczatek, h)),
                fabs(backward(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0),
                fabs(backward_3p(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0),
                fabs(central(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0),
                fabs(forward(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0),
                fabs(forward_3p(srodek, h) + (T) sqrt((T)2.0) / (T) 2.0),
                fabs(backward(koniec, h) + (T) 1.0),
                fabs(backward_3p(koniec, h) +(T) 1.0));
    }

	fclose(file);
}

int main()
{
    
    //~ Pochodna<float>().policz();
    Pochodna<double>().policz();

}
